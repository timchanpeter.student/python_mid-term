from cgitb import text
from tkinter import *
from tkinter import messagebox

def Ok():
    uname = e1.get()
    password = e2.get()
 
    if(uname == "" and password == "") :
        messagebox.showinfo("Message", "Blank Not allowed")
 
 
    elif(uname == "Admin" and password == "123"):
 
        messagebox.showinfo("Message","Logged Successfully")
        
 
    else :
        messagebox.showinfo("Message","Incorrent Username and Password")

def quit(root):
    root.destroy() 
 
root = Tk()
root.title("Login")
root.geometry("300x200")

global e1
global e2

Label(root, text="Verified Account", width=25, font=("bold", 15)).place(x=10, y=10)
Label(root, text="Username").place(x=10, y=50)
Label(root, text="Password").place(x=10, y=90)
 
e1 = Entry(root)
e1.place(x=140, y=50)
 
e2 = Entry(root)
e2.place(x=140, y=90)
e2.config(show="*")

Button(root, text="Cancel", command=lambda root=root:quit(root),height = 1, width = 10).place(x=10, y=130)
Button(root, text="Verify", command=Ok ,height = 1, width = 20).place(x=115, y=130)
 
root.mainloop()