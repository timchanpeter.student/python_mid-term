from logging import root
from tkinter import *
from tkinter.ttk import Treeview


root = root
root = Tk()
root. title('Database')
root.resizable(0,0)
root.state("zoomed")

set = Treeview(root)
set.pack(fill=BOTH, expand=1)

set['columns']= ('id','full_Name','gender','job_title','status')
set.column("#0", width=0, stretch=NO)
set.column("id",anchor=CENTER,width=80)
set.column("full_Name",anchor=CENTER, width=80)
set.column("gender",anchor=CENTER, width=80)
set.column("job_title",anchor=CENTER,width=80)
set.column("status",anchor=CENTER, width=80)

set.heading("#0",text="",anchor=CENTER)
set.heading("id",text="ID",anchor=CENTER)
set.heading("full_Name",text="FULL NAME",anchor=CENTER)
set.heading("gender",text="GENDER",anchor=CENTER)
set.heading("job_title",text="JOB TITLE",anchor=CENTER)
set.heading("status",text="STATUS",anchor=CENTER)

set.insert(parent='',index='end',iid=0,text='',
values=('001','Sokmaly','Male','Student','Free'))
set.insert(parent='',index='end',iid=1,text='',
values=('002','Wath','Male','Student','Free'))
set.insert(parent='',index='end',iid=2,text='',
values=('003','Peter','Male','Student','Free'))
set.insert(parent='',index='end',iid=3,text='',
values=('004','thiaReuth','Male','Student','Free'))
set.insert(parent='',index='end',iid=4,text='',
values=('005','Piseth','Male','Student','Busy'))
set.insert(parent='',index='end',iid=5,text='',
values=('006','Panha','Male','Student','Busy'))
set.insert(parent='',index='end',iid=6,text='',
values=('007','Thanarath','Male','Student','Busy'))

root.mainloop()